#!/usr/bin/env python
__author__ = 'prost'

import easy_plugins

TEST_DATA = {
    "path1": ["prefix_a", "prefix_b", "prase_a", "prefix_y.jpg"],
    "path2": ["prefix_c.py", "prefix_d", "prase_b", "prase_c", "prefix_not_dir"]
}

NOT_DIRS = ["prefix_y.jpg", "prefix_c.py", "prefix_not_dir"]

class DummyModule(object): pass

class TestEasyPlugins(object):
    @classmethod
    def setup_class(cls):
        """
        Change a few items in standard lib.
        """
        cls.os_old = easy_plugins.os
        cls.sys_old = easy_plugins.sys
        cls._import_old = easy_plugins._import

        easy_plugins.os = DummyModule()
        easy_plugins.os.path = DummyModule()
        easy_plugins.sys = DummyModule()

        easy_plugins.os.listdir = lambda x: TEST_DATA[x]
        easy_plugins.os.path.isdir = lambda x: x not in NOT_DIRS
        easy_plugins.os.path.join = lambda path, name: name
        easy_plugins.sys.path = TEST_DATA.keys()

        easy_plugins._import = lambda x: x

    @classmethod
    def teardown_class(cls):
        easy_plugins.os = cls.os_old
        easy_plugins.sys = cls.sys_old
        easy_plugins._import = cls._import_old

    def test_single_run(self):
        plugins = easy_plugins.EasyPlugins("prefix_")

        result = {}
        for name, plugin in plugins:
            result[name] = plugin

        assert(len(result) == 4)
        assert(result["prefix_a"] == "prefix_a")
        assert(result["prefix_b"] == "prefix_b")
        assert(result["prefix_c"] == "prefix_c")
        assert(result["prefix_d"] == "prefix_d")

    def test_multiple_runs(self):
        plugins = easy_plugins.EasyPlugins("prefix_")
        result = {}

        for name, plugin in plugins:
            result[name] = plugin

        assert(len(result) == 4)
        assert(result["prefix_a"] == "prefix_a")
        assert(result["prefix_b"] == "prefix_b")
        assert(result["prefix_c"] == "prefix_c")
        assert(result["prefix_d"] == "prefix_d")

        result = {}

        for name, plugin in plugins:
            result[name] = plugin

        assert(len(result) == 4)
        assert(result["prefix_a"] == "prefix_a")
        assert(result["prefix_b"] == "prefix_b")
        assert(result["prefix_c"] == "prefix_c")
        assert(result["prefix_d"] == "prefix_d")